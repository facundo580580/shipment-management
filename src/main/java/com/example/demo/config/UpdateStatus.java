package com.example.demo.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.demo.repository.EStatus;
import com.example.demo.repository.interfaces.ShipmentRepository;
import com.example.demo.repository.models.Shipment;

@Service
public class UpdateStatus {

   private final ShipmentRepository shipmentrepository;

   @Autowired
   public UpdateStatus(ShipmentRepository shipmentrepository) {
      this.shipmentrepository = shipmentrepository;
   }

   @Scheduled(fixedDelayString = "10000")
      public void upgradeTaskStatus() {
         List<Shipment> shipments = shipmentrepository.findByStatusNot(EStatus.RECEIVED);
         for (Shipment shipment : shipments){
            shipment.setStatus(shipment.getStatus().getNextStatus());
            shipmentrepository.save(shipment);
         }
      }
   }



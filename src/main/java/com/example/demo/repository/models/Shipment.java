package com.example.demo.repository.models;

import java.io.Serializable;

import com.example.demo.repository.EStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "shipment")
public class Shipment implements Serializable {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private int idShipment;

   @Column(name = "tracking_number")
   private String trackingNumber;

   @Enumerated(EnumType.STRING)
   private EStatus status;

   @ManyToOne
   @JoinColumn(name = "id_person")
   private Person person;

}
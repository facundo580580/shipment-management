package com.example.demo.repository.interfaces;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.repository.EStatus;
import com.example.demo.repository.models.Person;
import com.example.demo.repository.models.Shipment;


@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Integer> {

   List<Shipment> findByStatusNot(EStatus status);

   Optional<Shipment> findByTrackingNumber(String trackingNumber);

   List<Shipment> findByPerson(Person person);

}


package com.example.demo.repository.interfaces;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.repository.models.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

   Optional<Person> findByDocument(String document);

}

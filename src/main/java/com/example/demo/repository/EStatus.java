package com.example.demo.repository;

public enum EStatus {

   PENDING,
   DELIVERED,
   RECEIVED;

   public EStatus getNextStatus() {
      return switch (this) {
         case PENDING -> DELIVERED;
         default -> RECEIVED;
      };
   }

}

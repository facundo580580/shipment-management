package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.repository.EStatus;
import com.example.demo.repository.interfaces.PersonRepository;
import com.example.demo.repository.interfaces.ShipmentRepository;
import com.example.demo.repository.models.Person;
import com.example.demo.repository.models.Shipment;
import com.example.demo.service.dto.CreateShipDto;
import com.example.demo.service.dto.CreateShipRS;
import com.example.demo.service.dto.ShipRS;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ShippingManagementService {

   private final PersonRepository personRepository;

   private final ShipmentRepository shipmenRepository;

   @Autowired
   public ShippingManagementService(PersonRepository personRepository, ShipmentRepository shipmentRepository){
      this.personRepository = personRepository;
      this.shipmenRepository = shipmentRepository;
   }

   public CreateShipRS createShip(CreateShipDto request){
      Optional<Person> personOptional = personRepository.findByDocument(request.getDocument());
      Shipment shipment = new Shipment();
      shipment.setTrackingNumber(generateTrackingNumber());
      shipment.setStatus(EStatus.PENDING);

      if (personOptional.isPresent()){
         shipment.setPerson(personOptional.get());
      }
      else {
         Person person = new Person();
         person.setFirstName(request.getFirstName());
         person.setLastName(request.getLastName());
         person.setDocument(request.getDocument());
         shipment.setPerson(personRepository.save(person));

      }
      shipmenRepository.save(shipment);
      return new CreateShipRS(shipment.getTrackingNumber());
   }

   public ResponseEntity<ShipRS> showShipment(String trackingNumber){
      Optional<Shipment> shipmentOptional = shipmenRepository.findByTrackingNumber(trackingNumber);
      ShipRS shipRS = new ShipRS();
      if (shipmentOptional.isPresent()){
         shipRS.setTrackingNumber(shipmentOptional.get().getTrackingNumber());
         shipRS.setStatus(shipmentOptional.get().getStatus());
      } else {
         return setErrorResponse("Shipment not found");
      }
      return new ResponseEntity<>(shipRS, HttpStatus.OK);
   }

   public List<ResponseEntity<ShipRS>> allShipments(String document){
      Optional<Person> personOptional = personRepository.findByDocument(document);
      ShipRS shipRS = new ShipRS();
      List<ShipRS> listShipRS = new ArrayList<>();
      List<ResponseEntity<ShipRS>> responseEntities = new ArrayList<>();
      if (personOptional.isPresent()){
         List<Shipment> shipments = shipmenRepository.findByPerson(personOptional.get());
         for(Shipment shipment : shipments){
            shipRS.setTrackingNumber(shipment.getTrackingNumber());
            shipRS.setStatus(shipment.getStatus());
            listShipRS.add(shipRS);
         }
         for (ShipRS rs : listShipRS) {
            responseEntities.add(ResponseEntity.ok(rs));
         }
      }
       else {
         shipRS.setError("No shipments found");
         responseEntities.add(ResponseEntity.status(HttpStatus.NOT_FOUND).body(shipRS));
      }
       return responseEntities;
   }

   private String generateTrackingNumber() {
      return UUID.randomUUID().toString();
   }
   private ResponseEntity<ShipRS> setErrorResponse(String reason) {
      ShipRS shipRS = new ShipRS();
      shipRS.setError(reason);
      return new ResponseEntity<>(shipRS, HttpStatus.NOT_FOUND);
   }

}
package com.example.demo.service.dto;

import lombok.Data;

@Data
public class CreateShipDto {

   private String firstName;

   private String lastName;

   private String document;

}

package com.example.demo.service.dto;

import com.example.demo.repository.EStatus;
import com.example.demo.repository.models.Person;

public class ShipRQ {

   private String trackingNumber;

   private EStatus status;

   private Person person;
}

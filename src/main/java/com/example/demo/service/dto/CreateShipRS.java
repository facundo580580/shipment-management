package com.example.demo.service.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateShipRS {

   private String trackingNumber;

}

package com.example.demo.service.dto;

import com.example.demo.repository.EStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShipRS {

   private EStatus status;

   private String trackingNumber;

   private String error;
}

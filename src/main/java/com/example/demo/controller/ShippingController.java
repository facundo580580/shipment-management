package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ShippingManagementService;
import com.example.demo.service.dto.CreateShipDto;
import com.example.demo.service.dto.CreateShipRS;
import com.example.demo.service.dto.ShipRQ;
import com.example.demo.service.dto.ShipRS;

@RestController
@RequestMapping("/api/v1")
public class ShippingController {

   @Autowired
   private ShippingManagementService shippingManagementService;

   @PostMapping("/shipments")
   public CreateShipRS createShipping(@RequestBody CreateShipDto request){
      return shippingManagementService.createShip(request);
   }

   @GetMapping("/tracking")
   public ResponseEntity<ShipRS> showShipment(@RequestParam String trackingNumber){
      return shippingManagementService.showShipment(trackingNumber);
   }

   @GetMapping("/allShipments")
   public List<ResponseEntity<ShipRS>> allShipments(@RequestParam String document){
      return shippingManagementService.allShipments(document);
   }
}
